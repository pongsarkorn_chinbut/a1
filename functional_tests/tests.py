from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys


class NewVisitorTest(LiveServerTestCase):

    def setUp(self):  # เปิด browser 
        self.browser = webdriver.Firefox()
        self.browser.implicitly_wait(3)

    def tearDown(self):  # ปิด browser
        self.browser.quit()

    def check_for_row_in_table(self, row_text):
        table = self.browser.find_element_by_id('electronics_data_table')
        rows = table.find_elements_by_tag_name('tr')
        self.assertIn(row_text, [row.text for row in rows])

    def test_can_see_a_title_and_insert_data(self):  
        self.browser.get(self.live_server_url) #john เข้ามาที่หน้าแรก

        self.assertIn('Electricity Charge Calculator', self.browser.title)  
        #เห็นคำว่า 'Electricity Charge Calculator' บน title

        # ใส่ข้อมูลของทีวี
        # ชื่อ
        electronic_name = self.browser.find_element_by_id('name_box')
        self.assertEqual(
                electronic_name.get_attribute('placeholder'),
                'New Electronic\'s Name'
        )
        electronic_name.send_keys('Television')

        # จำนวน
        number = self.browser.find_element_by_id('number_box')
        self.assertEqual(
                number.get_attribute('placeholder'),
                'Number'
        )
        number.send_keys('1')

        # กำลังไฟฟ้า
        power = self.browser.find_element_by_id('power_box')
        self.assertEqual(
                power.get_attribute('placeholder'),
                'Power (watts)'
        )
        power.send_keys('100')

        # เวลา
        time = self.browser.find_element_by_id('time_box')
        self.assertEqual(
                time.get_attribute('placeholder'),
                'Time (hours a day)'
        )
        time.send_keys('6')

        # กดปุ่มส่งข้อมูล
        self.browser.find_element_by_id('submit_data').click()


        john_list_url = self.browser.current_url
        self.assertRegex(john_list_url, '/electricity_charge/.+')
        self.check_for_row_in_table('Television 1 100 6.0 0.6000000000000001')


        # ใส่ข้อมูลของคอมพิวเตอร์
        # ชื่อ
        electronic_name = self.browser.find_element_by_id('name_box')
        self.assertEqual(
                electronic_name.get_attribute('placeholder'),
                'New Electronic\'s Name'
        )
        electronic_name.send_keys('Personal Computer')

        # จำนวน
        number = self.browser.find_element_by_id('number_box')
        self.assertEqual(
                number.get_attribute('placeholder'),
                'Number'
        )
        number.send_keys('1')

        # กำลังไฟฟ้า
        power = self.browser.find_element_by_id('power_box')
        self.assertEqual(
                power.get_attribute('placeholder'),
                'Power (watts)'
        )
        power.send_keys('400')

        # เวลา
        time = self.browser.find_element_by_id('time_box')
        self.assertEqual(
                time.get_attribute('placeholder'),
                'Time (hours a day)'
        )
        time.send_keys('8')

        # กดปุ่มส่งข้อมูล
        self.browser.find_element_by_id('submit_data').click()

        import time
        time.sleep(3)
        self.check_for_row_in_table('Television 1 100 6.0 0.6000000000000001')
        self.check_for_row_in_table('Personal Computer 1 400 8.0 3.2')

        #ผู้ใช้คนใหม่เข้ามาชื่อ jack จะต้องไม่มีข้อมูลเครื่องใช้ไฟฟ้าของ john อยู่
        self.browser.quit()
        self.browser = webdriver.Firefox()

        self.browser.get(self.live_server_url)
        page_text = self.browser.find_element_by_tag_name('body').text
        self.assertNotIn('Television 1 100 6.0 0.6000000000000001', page_text)
        self.assertNotIn('Personal Computer 1 400 8.0 3.2', page_text)

        # jack ใส่ข้อมูลของพัดลม
        # ชื่อ
        electronic_name = self.browser.find_element_by_id('name_box')
        self.assertEqual(
                electronic_name.get_attribute('placeholder'),
                'New Electronic\'s Name'
        )
        electronic_name.send_keys('Fan')

        # จำนวน
        number = self.browser.find_element_by_id('number_box')
        self.assertEqual(
                number.get_attribute('placeholder'),
                'Number'
        )
        number.send_keys('3')

        # กำลังไฟฟ้า
        power = self.browser.find_element_by_id('power_box')
        self.assertEqual(
                power.get_attribute('placeholder'),
                'Power (watts)'
        )
        power.send_keys('100')

        # เวลา
        time = self.browser.find_element_by_id('time_box')
        self.assertEqual(
                time.get_attribute('placeholder'),
                'Time (hours a day)'
        )
        time.send_keys('6')

        # กดปุ่มส่งข้อมูล
        self.browser.find_element_by_id('submit_data').click()

        import time
        time.sleep(3)
        jack_list_url = self.browser.current_url
        self.assertRegex(jack_list_url, '/electricity_charge/.+')
        self.assertNotEqual(jack_list_url, john_list_url)
         
        #ตรวจสอบอีกครั้งว่ามีเฉพาะข้อมูลของ jack ไม่มีของ john ในตาราง
        page_text = self.browser.find_element_by_tag_name('body').text
        self.assertNotIn('Television', page_text)
        self.assertNotIn('Personal Computer', page_text)
        self.assertIn('Fan', page_text)

        self.fail('Finish the test!')
