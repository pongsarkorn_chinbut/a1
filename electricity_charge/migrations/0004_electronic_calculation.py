# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('electricity_charge', '0003_calculation'),
    ]

    operations = [
        migrations.AddField(
            model_name='electronic',
            name='calculation',
            field=models.ForeignKey(default=None, to='electricity_charge.Calculation'),
            preserve_default=True,
        ),
    ]
