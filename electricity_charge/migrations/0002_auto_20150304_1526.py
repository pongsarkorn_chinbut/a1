# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('electricity_charge', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='electronic',
            name='name',
            field=models.TextField(default=''),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='electronic',
            name='number',
            field=models.IntegerField(default=0),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='electronic',
            name='power',
            field=models.IntegerField(default=0),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='electronic',
            name='time',
            field=models.FloatField(default=0),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='electronic',
            name='unit',
            field=models.FloatField(default=0),
            preserve_default=True,
        ),
    ]
