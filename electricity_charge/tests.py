from django.core.urlresolvers import resolve
from django.test import TestCase
from django.http import HttpRequest
from electricity_charge.views import home_page
from django.template.loader import render_to_string
from electricity_charge.models import Electronic, Calculation

class HomePageTest(TestCase):

    def test_root_url_resolves_to_home_page_view(self):
        found = resolve('/')
        self.assertEqual(found.func, home_page)

    def test_home_page_returns_correct_html(self):
        request = HttpRequest()
        response = home_page(request)
        expected_html = render_to_string('home.html',
            {
         #    'number_text':  '0',
         #    'power_text':  '0',
         #    'time_text':  '0.0',
         #    'unit_text': '0.0',
            'sum_unit' : '0',
            'cost_unit' : '0.0',
            'ft' : '10.04',
            'cost_add_ft' : '0.0',
            'vat' : '0.0',
            'net_cost' : '0.0',
        })
        self.assertEqual(response.content.decode(), expected_html)


class CalculationAndElectronicModelTest(TestCase):

    def test_can_save_datas_and_show_all(self):
        calculation_ = Calculation()
        calculation_.save()

        first_electronic = Electronic()
        first_electronic.name = 'fan'
        first_electronic.number = 4
        first_electronic.power = 150
        first_electronic.time = 10    
        first_electronic.unit = 6
        first_electronic.calculation = calculation_
        first_electronic.save()

        second_electronic = Electronic()
        second_electronic.name = 'fluorescent lamp'
        second_electronic.number = 7
        second_electronic.power = 100
        second_electronic.time = 6    
        second_electronic.unit = 4.2
        second_electronic.calculation = calculation_   
        second_electronic.save()
  
        saved_calculation = Calculation.objects.first()
        self.assertEqual(saved_calculation, calculation_)

        saved_electronics = Electronic.objects.all()
        self.assertEqual(saved_electronics.count(), 2)

        first_saved_electronic = saved_electronics[0]
        second_saved_electronic = saved_electronics[1]
        self.assertEqual(first_saved_electronic.name, 'fan')
        self.assertEqual(first_saved_electronic.number, 4)
        self.assertEqual(first_saved_electronic.power, 150)
        self.assertEqual(first_saved_electronic.time, 10)
        self.assertEqual(first_saved_electronic.unit, 6)
        self.assertEqual(first_saved_electronic.calculation, calculation_)

        self.assertEqual(second_saved_electronic.name, 'fluorescent lamp')
        self.assertEqual(second_saved_electronic.number, 7)
        self.assertEqual(second_saved_electronic.power, 100)
        self.assertEqual(second_saved_electronic.time, 6)
        self.assertEqual(second_saved_electronic.unit, 4.2)
        self.assertEqual(second_saved_electronic.calculation, calculation_)

class NewCalculationTest(TestCase):

    def test_saving_a_POST_request(self):
        self.client.post(
            '/electricity_charge/new',{
                'name_text':  'Personal Computer',
                'number_text':  1,
                'power_text':  350,
                'time_text':  6.0,
            }
        )
        self.assertEqual(Electronic.objects.count(), 1)
        new_item = Electronic.objects.first()
        self.assertEqual(new_item.name, 'Personal Computer')
        self.assertEqual(new_item.number, 1)
        self.assertEqual(new_item.power, 350)
        self.assertEqual(new_item.time, 6.0)

    def test_home_page_redirects_after_POST(self):
        response = self.client.post(
            '/electricity_charge/new',{
                'name_text':  'Personal Computer',
                'number_text':  '1',
                'power_text':  '350',
                'time_text':  '6.0',
            }
        )
        new_calculation = Calculation.objects.first()
        self.assertRedirects(response, '/electricity_charge/%d/' % (new_calculation.id,))

class CalculationViewTest(TestCase):

    def test_uses_electricity_charge_template(self):
        calculation_ = Calculation.objects.create()
        response = self.client.get('/electricity_charge/%d/' % (calculation_.id,))
        self.assertTemplateUsed(response, 'electricity_charge.html')


    def test_displays_only_items_for_that_list(self):
        correct_calculation = Calculation.objects.create()
        Electronic.objects.create(name='elec 1', calculation=correct_calculation)
        Electronic.objects.create(name='elec 2', calculation=correct_calculation)
        other_calculation = Calculation.objects.create()
        Electronic.objects.create(name='other elec1', calculation=other_calculation)
        Electronic.objects.create(name='other elec2', calculation=other_calculation)

        response = self.client.get('/electricity_charge/%d/' % (correct_calculation.id,))

        self.assertContains(response, 'elec 1')
        self.assertContains(response, 'elec 2')
        self.assertNotContains(response, 'other elec1')
        self.assertNotContains(response, 'other elec2')

    def test_passes_correct_calculation_to_template(self):
        other_calculation = Calculation.objects.create()
        correct_calculation = Calculation.objects.create()
        response = self.client.get('/electricity_charge/%d/' % (correct_calculation.id,))
        self.assertEqual(response.context['calculation'], correct_calculation)

class NewElectronicsTest(TestCase):

    def test_can_save_a_POST_request_to_an_existing_list(self):
        other_calculation = Calculation.objects.create()
        correct_calculation = Calculation.objects.create()

        self.client.post(
            '/electricity_charge/%d/add_electronic' % (correct_calculation.id,),
            data={'name_text': 'A new electronic for an existing list',
                  'number_text' : '4',
                  'power_text' : '200',
                  'time_text' : '6'}
        )

        self.assertEqual(Electronic.objects.count(), 1)
        new_electronic = Electronic.objects.first()
        self.assertEqual(new_electronic.name, 'A new electronic for an existing list')
        self.assertEqual(new_electronic.calculation, correct_calculation)


    def test_redirects_to_calculation_view(self):
        other_calculation = Calculation.objects.create()
        correct_calculation = Calculation.objects.create()

        response = self.client.post(
            '/electricity_charge/%d/add_electronic' % (correct_calculation.id,),
            data={'name_text': 'A new electronic for an existing list',
                  'number_text' : '4',
                  'power_text' : '200',
                  'time_text' : '6'}
        )

        self.assertRedirects(response, '/electricity_charge/%d/' % (correct_calculation.id,))

class ElectricityCalculateTest(TestCase):
    def test_calculate_collect(self):
        response = self.client.post(
            '/electricity_charge/new',{
                'name_text':  'A new electronic',
                'number_text':  3,
                'power_text':  100,
                'time_text':  6.0,
            }
        )
        self.assertEqual(Electronic.objects.count(), 1)
        new_electronic = Electronic.objects.first()
        self.assertEqual(new_electronic.unit, 1.8000000000000003)
        #self.assertIn('A new electronic', response.content.decode())
        expected_html = render_to_string('electricity_charge.html',
            {
             'number_text':  'A new electronic',
             'number_text' : '3',
             'power_text':  '100',
             'time_text':  '6.0',
             'unit_text': '1.8000000000000003',
             'sum_unit' : '1.8000000000000003',
             'cost_unit' : '3.2484600000000006',
             'ft' : '10.04',
             'cost_add_ft' : '3.4291800000000006',
             'vat' : '0.2400426000000002',
             'net_cost' : '3.6692226000000003',
        })

        self.assertEqual(response.content.decode(), expected_html)
