from django.db import models

class Calculation(models.Model):
    pass

class Electronic(models.Model):
    name = models.TextField(default='')
    number = models.IntegerField(default=0)
    power = models.IntegerField(default=0)
    time = models.FloatField(default=0)
    unit = models.FloatField(default=0)
    calculation = models.ForeignKey(Calculation, default=None)

