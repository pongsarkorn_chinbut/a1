from django.shortcuts import redirect, render
from electricity_charge.models import Electronic, Calculation
from sys import maxsize

def home_page(request):
     return render(request, 'home.html',{
            'sum_unit' : '0',
            'cost_unit' : '0.0',
            'ft' : '10.04',
            'cost_add_ft' : '0.0',
            'vat' : '0.0',
            'net_cost' : '0.0',
    })

def view_calculation(request, calculation_id):
    calculation_ = Calculation.objects.get(id=calculation_id)
    electronics = Electronic.objects.filter(calculation=calculation_)
    sum_unit = 0
    for elec in electronics:
        sum_unit = sum_unit + elec.unit

    cost_unit = 0
    if sum_unit < 150:
        cost_unit = cost_less_than_150_units_a_month(sum_unit)
    else:
        cost_unit = cost_more_than_150_units_a_month(sum_unit)

    ft = 10.04
    cost_add_ft = cost_unit + (sum_unit*ft/100)

    return render(request, 'electricity_charge.html',{
        'electronics' : electronics,
        'sum_unit' : sum_unit,
        'cost_unit' : cost_unit,
        'ft' : ft,
        'cost_add_ft' : cost_add_ft,
        'vat' : 7*(cost_add_ft)/100,
        'net_cost' : 107*(cost_add_ft)/100,
        'calculation' : calculation_,
    })

def new_calculation(request):
    calculation_ = Calculation.objects.create()
    Electronic.objects.create(
        name=request.POST['name_text'],
        number=int(request.POST['number_text']),
        power=int(request.POST['power_text']),
        time=float(request.POST['time_text']),
        unit= float((int(request.POST['power_text'])/1000)*int(request.POST['number_text'])*float(request.POST['time_text'])),
        calculation=calculation_)
    return redirect('/electricity_charge/%d/' % (calculation_.id,))


def add_electronic(request, calculation_id):
    calculation_ = Calculation.objects.get(id=calculation_id)
    Electronic.objects.create(
        name=request.POST['name_text'],
        number=int(request.POST['number_text']),
        power=int(request.POST['power_text']),
        time=float(request.POST['time_text']),
        unit= float((int(request.POST['power_text'])/1000)*int(request.POST['number_text'])*float(request.POST['time_text'])),
        calculation=calculation_)
    return redirect('/electricity_charge/%d/' % (calculation_.id,))
  
def cost_less_than_150_units_a_month(unit):
    cost_per_unit = [1.3576,1.5445,1.7968,2.1800,2.2734,2.7781,2.9780]
    unit_rate = [5,10,10,10,65,250,maxsize]
    cost = 0
    rate_index = 0

    while unit > 0:
        if unit - unit_rate[rate_index] > 0:
            unit_of_this_cost = unit_rate[rate_index]
        else:
            unit_of_this_cost = unit
        cost = cost + (unit_of_this_cost*cost_per_unit[rate_index])
        unit = unit - unit_rate[rate_index]
        if rate_index < 6:
            rate_index = rate_index +1
        else:
            rate_index = 6
    return cost + 8.19 #บวกค่าบริการรายเดือน

def cost_more_than_150_units_a_month(unit):
    cost_per_unit = [1.8047,2.7781,2.9780]
    unit_rate = [150,400,maxsize]
    cost = 0
    rate_index = 0

    while unit > 0:
        if unit - unit_rate[rate_index] > 0:
            unit_of_this_cost = unit_rate[rate_index]
        else:
            unit_of_this_cost = unit
        cost = cost + (unit_of_this_cost*cost_per_unit[rate_index])
        unit = unit - unit_rate[rate_index]
        if rate_index < 2:
            rate_index = rate_index +1
        else:
            rate_index = 2
    return cost + 40.90 #บวกค่าบริการรายเดือน
