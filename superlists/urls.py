from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    url(r'^$', 'electricity_charge.views.home_page', name='home'),
    url(r'^electricity_charge/(\d+)/$', 'electricity_charge.views.view_calculation',
        name='view_calculation'
    ),
    url(r'^electricity_charge/(\d+)/add_electronic$', 'electricity_charge.views.add_electronic',
        name='add_electronic'
    ),
    url(r'^electricity_charge/new$', 'electricity_charge.views.new_calculation', name='new_calculation'),
    # url(r'^admin/', include(admin.site.urls)),
)
